package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.DestructionAct;
import kz.aitu.advancedJava.repository.DestructionActRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DestructionActService {

    public final DestructionActRepository destructionActRepository;

    public DestructionActService(DestructionActRepository destructionActRepository){
        this.destructionActRepository = destructionActRepository;
    }

    public List<DestructionAct> getAll(){
        return (List<DestructionAct>) destructionActRepository.findAll();
    }

    public DestructionAct getById(Long id){
        return destructionActRepository.findById(id).orElse(null);
    }

    public DestructionAct create(DestructionAct destructionAct){
        return destructionActRepository.save(destructionAct);
    }

    public DestructionAct update(DestructionAct catalog){
        return destructionActRepository.save(catalog);
    }

    public void delete(Long id){
        destructionActRepository.deleteById(id);
    }
}
