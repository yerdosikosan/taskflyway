package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.Cases;
import kz.aitu.advancedJava.repository.CaseRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaseService {
    public final CaseRepository caseRepository;

    public CaseService(CaseRepository caseRepository){
        this.caseRepository = caseRepository;
    }

    public List<Cases> getAll(){
        return (List<Cases>) caseRepository.findAll();
    }

    public Cases getById(Long id){
        return caseRepository.findById(id).orElse(null);
    }

    public Cases create(Cases cases){
        return caseRepository.save(cases);
    }

    public Cases update(Cases cases){
        return caseRepository.save(cases);
    }

    public void delete(Long id){
        caseRepository.deleteById(id);
    }

}
