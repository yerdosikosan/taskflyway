package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.ActivityJournal;
import kz.aitu.advancedJava.model.Authorization;
import kz.aitu.advancedJava.repository.ActivityJournalRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityJournalService {
    public final ActivityJournalRepository activityJournalRepository;

    public ActivityJournalService(ActivityJournalRepository activityJournalRepository){
        this.activityJournalRepository = activityJournalRepository;
    }

    public List<ActivityJournal> getAll() {
        return (List<ActivityJournal>) activityJournalRepository.findAll();
    }

    public ActivityJournal getById(Long id){
        return activityJournalRepository.findById(id).orElse(null);
    }
    public ActivityJournal create(ActivityJournal activityJournal){
        return activityJournalRepository.save(activityJournal);
    }

    public ActivityJournal update(ActivityJournal activityJournal){
        return activityJournalRepository.save(activityJournal);
    }

    public void delete(Long id){
        activityJournalRepository.deleteById(id);
    }

}
