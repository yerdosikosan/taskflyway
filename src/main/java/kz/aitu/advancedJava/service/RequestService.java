package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.Request;
import kz.aitu.advancedJava.repository.RequestRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestService {
    public final RequestRepository recordRepository;

    public RequestService(RequestRepository recordRepository){
        this.recordRepository = recordRepository;
    }

    public List<Request> getAll() {
        return (List<Request>) recordRepository.findAll();
    }

    public Request getById(Long id) {
        return recordRepository.findById(id).orElse(null);
    }

    public Request create(Request request) {
        return recordRepository.save(request);
    }

    public Request update(Request request) {
        return recordRepository.save(request);
    }

    public void delete(Long id) {
        recordRepository.deleteById(id);
    }
}
