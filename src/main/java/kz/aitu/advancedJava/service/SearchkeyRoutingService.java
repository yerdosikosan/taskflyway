package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.SearchkeyRouting;
import kz.aitu.advancedJava.repository.SearchkeyRoutingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchkeyRoutingService {
    public final SearchkeyRoutingRepository searchkeyRoutingRepository;

    public SearchkeyRoutingService( SearchkeyRoutingRepository searchkeyRoutingRepository){
        this.searchkeyRoutingRepository = searchkeyRoutingRepository;
    }

    public List<SearchkeyRouting> getAll() {
        return (List<SearchkeyRouting>) searchkeyRoutingRepository.findAll();
    }

    public SearchkeyRouting getById(Long id) {
        return searchkeyRoutingRepository.findById(id).orElse(null);
    }

    public SearchkeyRouting create(SearchkeyRouting searchkeyRouting) {
        return searchkeyRoutingRepository.save(searchkeyRouting);
    }

    public SearchkeyRouting update(SearchkeyRouting searchkeyRouting) {
        return searchkeyRoutingRepository.save(searchkeyRouting);
    }

    public void delete(Long id) {
        searchkeyRoutingRepository.deleteById(id);
    }
}
