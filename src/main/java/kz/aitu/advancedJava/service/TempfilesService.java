package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.Tempfiles;
import kz.aitu.advancedJava.repository.TempfilesRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TempfilesService {
    public final TempfilesRepository tempfilesRepository;

    public TempfilesService(TempfilesRepository tempfilesRepository){
        this.tempfilesRepository = tempfilesRepository;
    }

    public List<Tempfiles> getAll() {
        return (List<Tempfiles>) tempfilesRepository.findAll();
    }

    public Tempfiles getById(Long id) {
        return tempfilesRepository.findById(id).orElse(null);
    }

    public Tempfiles create(Tempfiles tempfiles) {
        return tempfilesRepository.save(tempfiles);
    }

    public Tempfiles update(Tempfiles tempfiles) {
        return tempfilesRepository.save(tempfiles);
    }

    public void delete(Long id) {
        tempfilesRepository.deleteById(id);
    }
}
