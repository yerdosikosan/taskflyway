package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.FileRouting;
import kz.aitu.advancedJava.repository.FileRoutingRepository;

import java.util.List;

public class FileRoutingService {
    public final FileRoutingRepository fileRoutingRepository;

    public FileRoutingService(FileRoutingRepository fileRoutingRepository){
        this.fileRoutingRepository = fileRoutingRepository;
    }

    public List<FileRouting> getAll(){
        return (List<FileRouting>) fileRoutingRepository.findAll();
    }

    public FileRouting getById(Long id){
        return fileRoutingRepository.findById(id).orElse(null);
    }

    public FileRouting create(FileRouting fileRouting){
        return fileRoutingRepository.save(fileRouting);
    }

    public FileRouting update(FileRouting fileRouting){
        return fileRoutingRepository.save(fileRouting);
    }

    public void delete(Long id){
        fileRoutingRepository.deleteById(id);
    }
}
