package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.DestructionAct;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DestructionActRepository extends CrudRepository<DestructionAct, Long> {
}
