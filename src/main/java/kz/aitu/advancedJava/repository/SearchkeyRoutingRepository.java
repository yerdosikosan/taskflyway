package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.SearchkeyRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchkeyRoutingRepository extends CrudRepository<SearchkeyRouting, Long> {
}
