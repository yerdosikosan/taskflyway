package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.NomenclatureSummary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NomenclatureSummaryRepository extends CrudRepository<NomenclatureSummary, Long> {
}
