package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.RequestStatusHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestStatusHistoryRepository extends CrudRepository<RequestStatusHistory, Long> {
}
