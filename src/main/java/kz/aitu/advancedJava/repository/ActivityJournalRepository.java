package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.ActivityJournal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityJournalRepository extends CrudRepository<ActivityJournal, Long> {
}
