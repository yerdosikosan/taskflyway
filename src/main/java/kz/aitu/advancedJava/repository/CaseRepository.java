package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Cases;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseRepository extends CrudRepository<Cases, Long> {
}
