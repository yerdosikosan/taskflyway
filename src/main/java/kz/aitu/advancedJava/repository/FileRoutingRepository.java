package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.FileRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRoutingRepository extends CrudRepository<FileRouting, Long>{

}