package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Authorization;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorizationRepository extends CrudRepository<Authorization, Long> {
}
