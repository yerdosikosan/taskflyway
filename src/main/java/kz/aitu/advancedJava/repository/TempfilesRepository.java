package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Tempfiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TempfilesRepository extends CrudRepository<Tempfiles, Long> {
}
