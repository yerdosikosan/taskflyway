package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.CaseIndex;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseIndexRepository extends CrudRepository<CaseIndex, Long> {
}
