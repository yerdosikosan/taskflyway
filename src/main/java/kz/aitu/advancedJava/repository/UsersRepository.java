package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Users;
import org.springframework.data.repository.CrudRepository;

public interface UsersRepository extends CrudRepository<Users, Long> {
}
