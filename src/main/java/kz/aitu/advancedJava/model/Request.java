package kz.aitu.advancedJava.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "request")
public class Request {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "request_user_id")
    private long requestUserId;

    @Column(name = "response_user_id")
    private long responseUserId;

    @Column(name = "case_id")
    private long caseId;

    @Column(name = "case_index_id")
    private long caseIndexId;

    @Column(name = "created_type")
    private String createdType;

    private String comment;

    private String status;

    private long timestamp;

    private long sharestart;

    private long sharefinish;

    private boolean favourite;

    @Column(name = "update_timestamp")
    private long updateTimestamp;

    @Column(name = "update_by")
    private long updateBy;

    private String declinenote;

    @Column(name = "company_unit_id")
    private long companyUnitId;

    @Column(name = "from_request_id")
    private long fromRequestId;

}
