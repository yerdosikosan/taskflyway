package kz.aitu.advancedJava.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class Users {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "auth_id")
    private long authId;

    private String name;

    private String fullname;

    private String surname;

    private String secondname;

    private String status;

    @Column(name = "company_unit_id")
    private long companyUnitId;

    private String password;

    @Column(name = "last_login_stamp")
    private long lastLoginStamp;

    private String iin;

    @Column(name = "is_active")
    private boolean isActive;

    @Column(name = "is_activated")
    private boolean isActivated;

    @Column(name = "created_timestamp")
    private long createdTimestamp;

    @Column(name = "created_by")
    private long createdBy;

    @Column(name = "updated_timestamp")
    private long updatedTimestamp;

    @Column(name = "updated_by")
    private long updatedBy;


}
