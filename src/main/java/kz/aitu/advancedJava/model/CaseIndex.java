package kz.aitu.advancedJava.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "case_index")
public class CaseIndex {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "case_index")
    private String caseIndex;

    @Column(name = "title_ru")
    private String titleRu;

    @Column(name = "title_kz")
    private String titleKz;

    @Column(name = "title_en")
    private String titleEn;

    @Column(name = "storage_type")
    private int storageType;

    @Column(name = "storage_year")
    private int storageYear;

    private String note;

    @Column(name = "company_unit_id")
    private long companyUnitId;

    @Column(name = "nomenclature_id")
    private long nomenclatureId;

    @Column(name = "created_timestamp")
    private long createdTimestamp;

    @Column(name = "created_by")
    private long createdBy;

    @Column(name = "updated_timestamp")
    private long updatedTimestamp;

    @Column(name = "updated_by")
    private long updatedBy;
}
