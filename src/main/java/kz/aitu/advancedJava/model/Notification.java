package kz.aitu.advancedJava.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "notification")
public class Notification {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "object_type")
    private String objectType;

    @Column(name = "object_id")
    private long objectId;

    @Column(name = "company_unit_id")
    private long companyUnitId;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "created_timestamp")
    private long createdTimestamp;

    @Column(name = "viewed_timestamp")
    private long viewedTimestamp;

    @Column(name = "is_viewed")
    private boolean isViewed;

    private String title;

    private String text;

    @Column(name = "company_id")
    private int companyId;
}
