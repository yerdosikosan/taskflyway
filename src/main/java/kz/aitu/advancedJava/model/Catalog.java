package kz.aitu.advancedJava.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "catalog")
public class Catalog {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name_rus")
    private String nameRus;

    @Column(name = "name_kz")
    private String nameKz;

    @Column(name = "name_en")
    private String nameEn;

    @Column(name = "parent_id")
    private long parentId;

    @Column(name = "company_unit_id")
    private long companyUnitId;

    @Column(name = "created_timestamp")
    private long createdTimestamp;

    @Column(name = "created_by")
    private long createdBy;

    @Column(name = "updated_timestamp")
    private long updatedTimestamp;

    @Column(name = "updated_by")
    private long updatedBy;
}
