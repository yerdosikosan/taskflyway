package kz.aitu.advancedJava.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "request_status_history")
public class RequestStatusHistory {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "request_id")
    private long requestId;

    private String status;

    @Column(name = "company_unit_id")
    private long companyUnitId;

    @Column(name = "created_timestamp")
    private long createdTimestamp;

    @Column(name = "created_by")
    private long createdBy;

    @Column(name = "updated_timestamp")
    private long updatedTimestamp;

    @Column(name = "updated_by")
    private long updatedBy;
}
