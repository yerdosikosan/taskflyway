package kz.aitu.advancedJava.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "searchkey_routing")
public class SearchkeyRouting {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "searchkey_id")
    private long searchkeyId;

    @Column(name = "table_name")
    private String tableName;

    @Column(name = "table_id")
    private long tableId;

    private String type;
}
