package kz.aitu.advancedJava.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "case")
public class Cases {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String number;

    @Column(name = "tom_number")
    private String tomNumber;

    @Column(name = "case_title_rus")
    private String caseTitleRus;

    @Column(name = "case_title_kz")
    private String caseTitleKz;

    @Column(name = "case_title_en")
    private String caseTitleEn;

    @Column(name = "starting_date")
    private long startingDate;

    @Column(name = "ending_date")
    private long endingDate;

    @Column(name = "page_count")
    private long pageCount;

    @Column(name = "eds_signature_flag")
    private boolean edsSignatureFlag;

    @Column(name = "eds_signature")
    private String edsSignature;

    @Column(name = "sending_sign_naf")
    private boolean sendingSignNaf;

    @Column(name = "deleting_sign")
    private boolean deletingSign;

    @Column(name = "limited_access_flag")
    private boolean limitedAccessFlag;

    private String hash;

    private int version;

    private String idversion;

    @Column(name = "version_activity_sign")
    private boolean versionActivitySign;

    private String note;

    @Column(name = "location_id")
    private long locationId;

    @Column(name = "case_index_id")
    private long caseIndexId;

    @Column(name = "record_id")
    private long recordId;

    @Column(name = "destruction_act_id")
    private long destructionActId;

    @Column(name = "company_unit_id")
    private long companyUnitId;

    @Column(name = "blockchain_case_address")
    private String blockchainCaseAddress;

    @Column(name = "blockchain_add_date")
    private long blockchainAddDate;

    @Column(name = "created_timestamp")
    private long createdTimestamp;

    @Column(name = "created_by")
    private long createdBy;

    @Column(name = "updated_timestamp")
    private long updatedTimestamp;

    @Column(name = "updated_by")
    private long updatedBy;


}
