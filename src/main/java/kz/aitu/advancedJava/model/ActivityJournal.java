package kz.aitu.advancedJava.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "activity_journal")
public class ActivityJournal {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "event_type")
    private String eventType;

    @Column(name = "object_type")
    private String objectType;

    @Column(name = "object_id")
    private long objectId;

    @Column(name = "company_unit_id")
    private long companyUnitId;

    @Column(name = "created_timestamp")
    private long createdTimestamp;

    @Column(name = "created_by")
    private long createdBy;

    @Column(name = "message_level")
    private String messageLevel;

    private String message;


}

