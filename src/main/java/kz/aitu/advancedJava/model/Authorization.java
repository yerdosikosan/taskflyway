package kz.aitu.advancedJava.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "auth")
public class Authorization {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String username;

    private String email;

    private String password;

    private String role;

    @Column(name = "forgot_password_key")
    private String forgotPasswordKey;

    @Column(name = "forgot_password_key_timestamp")
    private long forgotPasswordKeyTimestamp;

    @Column(name = "company_unit_id")
    private long companyUnitId;
}
