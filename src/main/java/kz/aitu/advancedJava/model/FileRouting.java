package kz.aitu.advancedJava.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "file_routing")
public class FileRouting {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "file_id")
    private long fileId;

    @Column(name = "table_name")
    private String tableName;

    @Column(name = "table_id")
    private long tableId;

    private String type;
}
