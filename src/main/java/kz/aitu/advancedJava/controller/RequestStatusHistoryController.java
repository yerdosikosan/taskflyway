package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.RequestStatusHistory;
import kz.aitu.advancedJava.service.RequestStatusHistoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RequestStatusHistoryController {
    private final RequestStatusHistoryService requestStatusHistoryService;

    public RequestStatusHistoryController(RequestStatusHistoryService requestStatusHistoryService) {
        this.requestStatusHistoryService = requestStatusHistoryService;
    }

    @GetMapping("/api/reques_status_history/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(requestStatusHistoryService.getById(id));
    }

    @GetMapping("/api/reques_status_history")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(requestStatusHistoryService.getAll());
    }

    @PostMapping("/api/reques_status_history")
    public ResponseEntity<?> save(@RequestBody RequestStatusHistory requestStatusHistory){
        return ResponseEntity.ok(requestStatusHistoryService.create(requestStatusHistory));
    }

    @PutMapping("/api/reques_status_history")
    public ResponseEntity<?> update(@RequestBody RequestStatusHistory requestStatusHistory){
        return ResponseEntity.ok(requestStatusHistoryService.create(requestStatusHistory));
    }

    @DeleteMapping("/api/reques_status_history/{id}")
    public void delete(@PathVariable Long id){
        requestStatusHistoryService.delete(id);
    }
}
