package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.SearchkeyRouting;
import kz.aitu.advancedJava.service.SearchkeyRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchkeyRoutingController {
    private final SearchkeyRoutingService searchkeyRoutingService;

    public SearchkeyRoutingController(SearchkeyRoutingService searchkeyRoutingService) {
        this.searchkeyRoutingService = searchkeyRoutingService;
    }

    @GetMapping("/api/searchkey_routing/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(searchkeyRoutingService.getById(id));
    }

    @GetMapping("/api/searchkey_routing")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(searchkeyRoutingService.getAll());
    }

    @PostMapping("/api/searchkey_routing")
    public ResponseEntity<?> save(@RequestBody SearchkeyRouting searchkeyRouting){
        return ResponseEntity.ok(searchkeyRoutingService.create(searchkeyRouting));
    }

    @PutMapping("/api/searchkey_routing")
    public ResponseEntity<?> update(@RequestBody SearchkeyRouting searchkeyRouting){
        return ResponseEntity.ok(searchkeyRoutingService.create(searchkeyRouting));
    }

    @DeleteMapping("/api/searchkey_routing/{id}")
    public void delete(@PathVariable Long id){
        searchkeyRoutingService.delete(id);
    }
}
