package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Catalog;
import kz.aitu.advancedJava.service.CatalogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogController {
    private final CatalogService catalogService;

    public CatalogController(CatalogService catalogService){
        this.catalogService = catalogService;
    }

    @GetMapping("/api/cataloge/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(catalogService.getById(id));
    }

    @GetMapping("/api/catalog")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(catalogService.getAll());
    }

    @PostMapping("/api/catalog")
    public ResponseEntity<?> save(@RequestBody Catalog catalog){
        return ResponseEntity.ok(catalogService.create(catalog));
    }

    @PutMapping("/api/catalog")
    public ResponseEntity<?> update(@RequestBody Catalog catalog){
        return ResponseEntity.ok(catalogService.create(catalog));
    }

    @DeleteMapping("/api/catalog/{id}")
    public void delete(@PathVariable Long id){
        catalogService.delete(id);
    }
}
