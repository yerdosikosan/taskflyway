package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.File;
import kz.aitu.advancedJava.service.FileService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileController {
    private final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }


    @GetMapping("/api/file/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(fileService.getById(id));
    }

    @GetMapping("/api/file")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(fileService.getAll());
    }

    @PostMapping("/api/file")
    public ResponseEntity<?> save(@RequestBody File file){
        return ResponseEntity.ok(fileService.create(file));
    }

    @PutMapping("/api/file")
    public ResponseEntity<?> update(@RequestBody File file){
        return ResponseEntity.ok(fileService.create(file));
    }

    @DeleteMapping("/api/file/{id}")
    public void delete(@PathVariable Long id){
        fileService.delete(id);
    }
}
