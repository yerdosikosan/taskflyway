package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Nomenclature;
import kz.aitu.advancedJava.service.NomenclatureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureController {
    private final NomenclatureService nomenclatureService;

    public NomenclatureController(NomenclatureService nomenclatureService) {
        this.nomenclatureService = nomenclatureService;
    }

    @GetMapping("/api/nomenclature/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(nomenclatureService.getById(id));
    }

    @GetMapping("/api/nomenclature")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(nomenclatureService.getAll());
    }

    @PostMapping("/api/nomenclature")
    public ResponseEntity<?> save(@RequestBody Nomenclature nomenclature){
        return ResponseEntity.ok(nomenclatureService.create(nomenclature));
    }

    @PutMapping("/api/nomenclature")
    public ResponseEntity<?> update(@RequestBody Nomenclature nomenclature){
        return ResponseEntity.ok(nomenclatureService.create(nomenclature));
    }

    @DeleteMapping("/api/nomenclature/{id}")
    public void delete(@PathVariable Long id){
        nomenclatureService.delete(id);
    }
}
