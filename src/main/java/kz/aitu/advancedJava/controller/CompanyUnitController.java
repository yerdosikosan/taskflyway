package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.CompanyUnit;
import kz.aitu.advancedJava.service.CompanyUnitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyUnitController {
    private final CompanyUnitService companyUnitService;

    public CompanyUnitController(CompanyUnitService companyUnitService) {
        this.companyUnitService = companyUnitService;
    }

    @GetMapping("/api/company_unit/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(companyUnitService.getById(id));
    }

    @GetMapping("/api/company_unit")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(companyUnitService.getAll());
    }

    @PostMapping("/api/company_unit")
    public ResponseEntity<?> save(@RequestBody CompanyUnit companyUnit) {
        return ResponseEntity.ok(companyUnitService.create(companyUnit));
    }

    @PutMapping("/api/company_unit")
    public ResponseEntity<?> update(@RequestBody CompanyUnit companyUnit) {
        return ResponseEntity.ok(companyUnitService.create(companyUnit));
    }

    @DeleteMapping("/api/company_unit/{id}")
    public void delete(@PathVariable Long id) {
        companyUnitService.delete(id);
    }
}
