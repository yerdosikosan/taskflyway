package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Record;
import kz.aitu.advancedJava.service.RecordService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RecordController {
    private final RecordService recordService;

    public RecordController(RecordService recordService) {
        this.recordService = recordService;
    }

    @GetMapping("/api/record/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(recordService.getById(id));
    }

    @GetMapping("/api/record")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(recordService.getAll());
    }

    @PostMapping("/api/record")
    public ResponseEntity<?> save(@RequestBody Record record){
        return ResponseEntity.ok(recordService.create(record));
    }

    @PutMapping("/api/record")
    public ResponseEntity<?> update(@RequestBody Record record){
        return ResponseEntity.ok(recordService.create(record));
    }

    @DeleteMapping("/api/record/{id}")
    public void delete(@PathVariable Long id){
        recordService.delete(id);
    }
}
