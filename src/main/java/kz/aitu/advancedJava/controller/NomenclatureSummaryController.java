package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.NomenclatureSummary;
import kz.aitu.advancedJava.service.NomenclatureSummaryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureSummaryController {
    private final NomenclatureSummaryService nomenclatureSummaryService;

    public NomenclatureSummaryController(NomenclatureSummaryService nomenclatureSummaryService) {
        this.nomenclatureSummaryService = nomenclatureSummaryService;
    }

    @GetMapping("/api/nomenclature_summary/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(nomenclatureSummaryService.getById(id));
    }

    @GetMapping("/api/nomenclature_summary")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(nomenclatureSummaryService.getAll());
    }

    @PostMapping("/api/nomenclature_summary")
    public ResponseEntity<?> save(@RequestBody NomenclatureSummary nomenclatureSummary){
        return ResponseEntity.ok(nomenclatureSummaryService.create(nomenclatureSummary));
    }

    @PutMapping("/api/nomenclature_summary")
    public ResponseEntity<?> update(@RequestBody NomenclatureSummary nomenclatureSummary){
        return ResponseEntity.ok(nomenclatureSummaryService.create(nomenclatureSummary));
    }

    @DeleteMapping("/api/nomenclature_summary/{id}")
    public void delete(@PathVariable Long id){
        nomenclatureSummaryService.delete(id);
    }
}
