package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.CatalogCase;
import kz.aitu.advancedJava.service.CatalogCaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogCaseController {
    private final CatalogCaseService catalogCaseService;

    public CatalogCaseController(CatalogCaseService catalogCaseService) {
        this.catalogCaseService = catalogCaseService;
    }

    @GetMapping("/api/catalog_case/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(catalogCaseService.getById(id));
    }

    @GetMapping("/api/catalog_case")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(catalogCaseService.getAll());
    }

    @PostMapping("/api/catalog_case")
    public ResponseEntity<?> save(@RequestBody CatalogCase catalogCase) {
        return ResponseEntity.ok(catalogCaseService.create(catalogCase));
    }

    @PutMapping("/api/catalog_case")
    public ResponseEntity<?> update(@RequestBody CatalogCase catalogCase) {
        return ResponseEntity.ok(catalogCaseService.create(catalogCase));
    }

    @DeleteMapping("/api/catalog_case/{id}")
    public void delete(@PathVariable Long id) {
        catalogCaseService.delete(id);
    }
}
