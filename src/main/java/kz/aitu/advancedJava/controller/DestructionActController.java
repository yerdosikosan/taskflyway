package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.DestructionAct;
import kz.aitu.advancedJava.service.DestructionActService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DestructionActController {
    private final DestructionActService destructionActService;

    public DestructionActController(DestructionActService destructionActService) {
        this.destructionActService = destructionActService;
    }


    @GetMapping("/api/destruction_act/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(destructionActService.getById(id));
    }

    @GetMapping("/api/destruction_act")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(destructionActService.getAll());
    }

    @PostMapping("/api/destruction_act")
    public ResponseEntity<?> save(@RequestBody DestructionAct destructionAct) {
        return ResponseEntity.ok(destructionActService.create(destructionAct));
    }

    @PutMapping("/api/destruction_act")
    public ResponseEntity<?> update(@RequestBody DestructionAct destructionAct) {
        return ResponseEntity.ok(destructionActService.create(destructionAct));
    }

    @DeleteMapping("/api/destruction_act/{id}")
    public void delete(@PathVariable Long id) {
        destructionActService.delete(id);
    }
}
