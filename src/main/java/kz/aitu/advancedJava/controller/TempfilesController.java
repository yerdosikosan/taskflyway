package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Tempfiles;
import kz.aitu.advancedJava.service.TempfilesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TempfilesController {
    private final TempfilesService tempfilesService;

    public TempfilesController(TempfilesService tempfilesService) {
        this.tempfilesService = tempfilesService;
    }

    @GetMapping("/api/tempfiles/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(tempfilesService.getById(id));
    }

    @GetMapping("/api/tempfiles")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(tempfilesService.getAll());
    }

    @PostMapping("/api/tempfiles")
    public ResponseEntity<?> save(@RequestBody Tempfiles tempfiles){
        return ResponseEntity.ok(tempfilesService.create(tempfiles));
    }

    @PutMapping("/api/tempfiles")
    public ResponseEntity<?> update(@RequestBody Tempfiles tempfiles){
        return ResponseEntity.ok(tempfilesService.create(tempfiles));
    }

    @DeleteMapping("/api/catempfilesse/{id}")
    public void delete(@PathVariable Long id){
        tempfilesService.delete(id);
    }
}
