package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Location;
import kz.aitu.advancedJava.service.LocationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public class LocationController {
    private final LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping("/api/location/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(locationService.getById(id));
    }

    @GetMapping("/api/location")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(locationService.getAll());
    }

    @PostMapping("/api/location")
    public ResponseEntity<?> save(@RequestBody Location location) {
        return ResponseEntity.ok(locationService.create(location));
    }

    @PutMapping("/api/location")
    public ResponseEntity<?> update(@RequestBody Location location) {
        return ResponseEntity.ok(locationService.create(location));
    }

    @DeleteMapping("/api/location/{id}")
    public void delete(@PathVariable Long id) {
        locationService.delete(id);
    }
}
