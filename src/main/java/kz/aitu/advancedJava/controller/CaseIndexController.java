package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.CaseIndex;
import kz.aitu.advancedJava.service.CaseIndexService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseIndexController {
    private final CaseIndexService caseIndexService;

    public CaseIndexController(CaseIndexService caseIndexService){
        this.caseIndexService = caseIndexService;
    }

    @GetMapping("/api/case_index/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(caseIndexService.getById(id));
    }

    @GetMapping("/api/case_index")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(caseIndexService.getAll());
    }

    @PostMapping("/api/case_index")
    public ResponseEntity<?> save(@RequestBody CaseIndex caseIndex){
        return ResponseEntity.ok(caseIndexService.create(caseIndex));
    }

    @PutMapping("/api/case_index")
    public ResponseEntity<?> update(@RequestBody CaseIndex caseIndex){
        return ResponseEntity.ok(caseIndexService.create(caseIndex));
    }

    @DeleteMapping("/api/case_index/{id}")
    public void delete(@PathVariable Long id){
        caseIndexService.delete(id);
    }
}
