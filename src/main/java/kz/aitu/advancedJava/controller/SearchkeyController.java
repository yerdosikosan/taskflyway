package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Searchkey;
import kz.aitu.advancedJava.service.SearchkeyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchkeyController {
    private final SearchkeyService searchkeyService;

    public SearchkeyController(SearchkeyService searchkeyService) {
        this.searchkeyService = searchkeyService;
    }

    @GetMapping("/api/searchkey/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(searchkeyService.getById(id));
    }

    @GetMapping("/api/searchkey")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(searchkeyService.getAll());
    }

    @PostMapping("/api/searchkey")
    public ResponseEntity<?> save(@RequestBody Searchkey searchkey){
        return ResponseEntity.ok(searchkeyService.create(searchkey));
    }

    @PutMapping("/api/searchkey")
    public ResponseEntity<?> update(@RequestBody Searchkey searchkey){
        return ResponseEntity.ok(searchkeyService.create(searchkey));
    }

    @DeleteMapping("/api/searchkey/{id}")
    public void delete(@PathVariable Long id){
        searchkeyService.delete(id);
    }
}
