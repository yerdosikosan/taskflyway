package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Users;
import kz.aitu.advancedJava.service.UsersService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UsersController {
    private final UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/api/users/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(usersService.getById(id));
    }

    @GetMapping("/api/users")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(usersService.getAll());
    }

    @PostMapping("/api/case")
    public ResponseEntity<?> save(@RequestBody Users users){
        return ResponseEntity.ok(usersService.create(users));
    }

    @PutMapping("/api/users")
    public ResponseEntity<?> update(@RequestBody Users users){
        return ResponseEntity.ok(usersService.create(users));
    }

    @DeleteMapping("/api/users/{id}")
    public void delete(@PathVariable Long id){
        usersService.delete(id);
    }
}
