package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Company;
import kz.aitu.advancedJava.service.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyController {
    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("/api/company/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(companyService.getById(id));
    }

    @GetMapping("/api/company")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(companyService.getAll());
    }

    @PostMapping("/api/company")
    public ResponseEntity<?> save(@RequestBody Company company) {
        return ResponseEntity.ok(companyService.create(company));
    }

    @PutMapping("/api/company")
    public ResponseEntity<?> update(@RequestBody Company company) {
        return ResponseEntity.ok(companyService.create(company));
    }

    @DeleteMapping("/api/company/{id}")
    public void delete(@PathVariable Long id) {
        companyService.delete(id);
    }
}
