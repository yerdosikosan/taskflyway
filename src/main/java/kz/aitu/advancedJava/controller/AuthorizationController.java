package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Authorization;
import kz.aitu.advancedJava.service.AuthorizationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthorizationController {
    private final AuthorizationService authorizationService;

    public AuthorizationController(AuthorizationService authorizationService){
        this.authorizationService = authorizationService;
    }

    @GetMapping("/api/authorization")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(authorizationService.getAll());
    }

    @GetMapping("/api/authorization/{autorization_id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(authorizationService.getById(id));
    }

    @PostMapping("/api/authorization")
    public ResponseEntity<?> save(@RequestBody Authorization authorization){
        return ResponseEntity.ok(authorizationService.create(authorization));
    }

    @PutMapping("/api/authorization")
    public ResponseEntity<?> update(@RequestBody Authorization authorization){
        return ResponseEntity.ok(authorizationService.create(authorization));
    }

    @DeleteMapping("/api/authorization/{authorization_id}")
    public void delete(@PathVariable Long id){
        authorizationService.delete(id);
    }


}
