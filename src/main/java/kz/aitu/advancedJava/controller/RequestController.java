package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Request;
import kz.aitu.advancedJava.service.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RequestController {
    private final RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @GetMapping("/api/request/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(requestService.getById(id));
    }

    @GetMapping("/api/request")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(requestService.getAll());
    }

    @PostMapping("/api/request")
    public ResponseEntity<?> save(@RequestBody Request request){
        return ResponseEntity.ok(requestService.create(request));
    }

    @PutMapping("/api/request")
    public ResponseEntity<?> update(@RequestBody Request request){
        return ResponseEntity.ok(requestService.create(request));
    }

    @DeleteMapping("/api/request/{id}")
    public void delete(@PathVariable Long id){
        requestService.delete(id);
    }
}
