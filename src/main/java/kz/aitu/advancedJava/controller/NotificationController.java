package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Notification;
import kz.aitu.advancedJava.service.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NotificationController {
    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("/api/notification/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(notificationService.getById(id));
    }

    @GetMapping("/api/notification")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(notificationService.getAll());
    }

    @PostMapping("/api/notification")
    public ResponseEntity<?> save(@RequestBody Notification notification){
        return ResponseEntity.ok(notificationService.create(notification));
    }

    @PutMapping("/api/notification")
    public ResponseEntity<?> update(@RequestBody Notification notification){
        return ResponseEntity.ok(notificationService.create(notification));
    }

    @DeleteMapping("/api/notification/{id}")
    public void delete(@PathVariable Long id){
        notificationService.delete(id);
    }
}
