package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Cases;
import kz.aitu.advancedJava.service.CaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseController {
    private final CaseService caseService;

    public CaseController(CaseService caseService){
        this.caseService = caseService;
    }

    @GetMapping("/api/case/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(caseService.getById(id));
    }

    @GetMapping("/api/case")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(caseService.getAll());
    }

    @PostMapping("/api/case")
    public ResponseEntity<?> save(@RequestBody Cases cases){
        return ResponseEntity.ok(caseService.create(cases));
    }

    @PutMapping("/api/case")
    public ResponseEntity<?> update(@RequestBody Cases cases){
        return ResponseEntity.ok(caseService.create(cases));
    }

    @DeleteMapping("/api/case/{id}")
    public void delete(@PathVariable Long id){
        caseService.delete(id);
    }
}
