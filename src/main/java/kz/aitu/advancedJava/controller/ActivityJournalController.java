package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.ActivityJournal;
import kz.aitu.advancedJava.service.ActivityJournalService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ActivityJournalController {
    private final ActivityJournalService activityJournalService;

    public ActivityJournalController(ActivityJournalService activityJournalService){
        this.activityJournalService = activityJournalService;
    }

    @GetMapping("/api/activity_journal/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(activityJournalService.getById(id));
    }

    @GetMapping("/api/activity_journal")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(activityJournalService.getAll());
    }

    @PostMapping("/api/activity_journal")
    public ResponseEntity<?> save(@RequestBody ActivityJournal activityJournal){
        return ResponseEntity.ok(activityJournalService.create(activityJournal));
    }

    @PutMapping("/api/activity_journal")
    public ResponseEntity<?> update(@RequestBody ActivityJournal activityJournal){
        return ResponseEntity.ok(activityJournalService.create(activityJournal));
    }

    @DeleteMapping("/api/activity_journal/{id}")
    public void delete(@PathVariable Long id){
        activityJournalService.delete(id);
    }
}
