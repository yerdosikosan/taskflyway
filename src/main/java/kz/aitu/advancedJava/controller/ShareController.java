package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Share;
import kz.aitu.advancedJava.service.ShareService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ShareController {
    private final ShareService shareService;

    public ShareController(ShareService shareService) {
        this.shareService = shareService;
    }

    @GetMapping("/api/share/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(shareService.getById(id));
    }

    @GetMapping("/api/share")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(shareService.getAll());
    }

    @PostMapping("/api/share")
    public ResponseEntity<?> save(@RequestBody Share share){
        return ResponseEntity.ok(shareService.create(share));
    }

    @PutMapping("/api/share")
    public ResponseEntity<?> update(@RequestBody Share share){
        return ResponseEntity.ok(shareService.create(share));
    }

    @DeleteMapping("/api/share/{id}")
    public void delete(@PathVariable Long id){
        shareService.delete(id);
    }
}
