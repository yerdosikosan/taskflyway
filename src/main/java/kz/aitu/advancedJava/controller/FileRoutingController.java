package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.FileRouting;
import kz.aitu.advancedJava.service.FileRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileRoutingController {
    private final FileRoutingService fileRoutingService;

    public FileRoutingController(FileRoutingService fileRoutingService) {
        this.fileRoutingService = fileRoutingService;
    }

    @GetMapping("/api/file_routing/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(fileRoutingService.getById(id));
    }

    @GetMapping("/api/file_routing")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(fileRoutingService.getAll());
    }

    @PostMapping("/api/file_routing")
    public ResponseEntity<?> save(@RequestBody FileRouting fileRouting){
        return ResponseEntity.ok(fileRoutingService.create(fileRouting));
    }

    @PutMapping("/api/file_routing")
    public ResponseEntity<?> update(@RequestBody FileRouting fileRouting){
        return ResponseEntity.ok(fileRoutingService.create(fileRouting));
    }

    @DeleteMapping("/api/file_routing/{id}")
    public void delete(@PathVariable Long id){
        fileRoutingService.delete(id);
    }
}
