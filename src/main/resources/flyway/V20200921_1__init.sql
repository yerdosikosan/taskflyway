drop table if exists fond;
create table fond(
id int(8) primary key,
fond_number varchar(128),
created_timestamp int(8),
created_by int(8),
updated_timestamp int(8),
updated_by int(8)
);
alter table groups owner to postgres;

drop table if exists company;
create table company(
id int(8) primary key,
name_ru varchar(128),
name_kz varchar(128),
name_en varchar(128),
bin varchar(32),
parent_id int(8),
fond_id int(8),
created_timestamp int(8),
created_by int(8),
updated_timestamp int(8),
updated_by int(8)
);
alter table groups owner to postgres;

drop table if exists company_unit;
create table company_unit(
id int(8) primary key,
name_ru varchar(128),
name_kz varchar(128),
name_en varchar(128),
parent_id int(8),
year int(4),
company_id int(8),
code_index varchar(16),
created_timestamp int(8),
created_by int(8),
updated_timestamp int(8),
updated_by int(8)
);
alter table groups owner to postgres;


drop table if exists auth;
create table auth(
id int(8) primary key,
username varchar(255),
email varchar(255),
password varchar(128),
role varchar(255),
forgot_password_key varchar(128),
forgot_password_key_timestamp int(8),
company_unit_id int(8)
);
alter table groups owner to postgres;


drop table if exists users;
create table users(
id int(8) primary key,
auth_id int(8),
name varchar(128),
fullname varchar(128),
surname varchar(128),
secondname varchar(128),
status varchar(128),
company_unit_id int(8),
password varchar(128),
last_login_timestamp int(8),
iin varchar(32),
is_active boolean,
is_activated boolean,
created_timestamp int(8),
created_by int(8),
updated_timestamp int(8),
updated_by int(8)
);
alter table groups owner to postgres;


drop table if exists share;
create table share(
id int(8) primary key,
request_id int(8),
note varchar(255),
sender_id int(8),
receiver_id int(8),
share_timestamp int(8)
);
alter table groups owner to postgres;


drop table if exists request;
create table request(
id int(8) primary key,
request_user_id int(8),
response_user_id int(8),
case_id int(8),
case_index_id int(8),
created_type varchar(64),
comment varchar(255),
status varchar(64),
timestamp int(8),
sharestart int(8),
sharefinish int(8),
favourite boolean,
update_timestamp int(8),
update_by int(8),
declinenote varchar(255),
company_unit_id int(8),
from_request_id int(8)
);
alter table groups owner to postgres;


drop table if exists request_status_history;
create table requestStatusHistory(
id int(8) primary key,
request_id int(8),
status varchar(64),
created_timestamp int(8),
created_by int(8),
updated_timestamp int(8),
updated_by int(8)
);
alter table groups owner to postgres;


drop table if exists case;
create table case(
id int(8) primary key,
number varchar(128),
tom_number varchar(128),
case_title_rus varchar(128),
case_title_kz varchar(128),
case_title_en varchar(128),
starting_date int(8),
ending_date int(8),
page_count int(8),
eds_signature_flag boolean,
eds_signature text,
sending_sign_naf boolean,
deleting_sign boolean,
limited_access_flag boolean,
hash varchar(128),
version int(4),
idversion varchar(128),
version_activity_sign boolean,
note varchar(225),
location_id int(8),
case_index_id int(8),
record_id int(8),
destruction_act_id int(8),
company_unit_id int(8),
blockchain_case_address varchar(255),
blockchain_add_date int(8),
created_timestamp int(8),
created_by int(8),
updated_timestamp int(8),
updated_by int(8)
);
alter table groups owner to postgres;


drop table if exists catalog;
create table catalog(
id int(8) primary key,
name_ru varchar(128),
name_kz varchar(128),
name_en varchar(128),
parent_id int(8),
company_unit_id int(8),
created_timestamp int(8),
created_by int(8),
updated_timestamp int(8),
updated_by int(8)
);
alter table groups owner to postgres;


drop table if exists catalog_case;
create table catalog_case(
id int(8) primary key,
case_id int(8),
catalog_id int(8),
created_timestamp int(8),
created_by int(8),
updated_timestamp int(8),
updated_by int(8)
);
alter table groups owner to postgres;


drop table if exists activity_journal;
create table activity_journal(
id int(8) primary key,
event_type varchar(128),
object_type varchar(128),
object_id int(8),
created_timestamp int(8),
created_by int(8),
message_level varchar(128),
message varchar(255)
);
alter table groups owner to postgres;


drop table if exists location;
create table location(
id int(8) primary key,
row varchar(64),
line varchar(64),
column varchar(64),
box varchar(64),
company_unit_id int(8),
created_timestamp int(8),
created_by int(8),
updated_timestamp int(8),
updated_by int(8)
);
alter table groups owner to postgres;


drop table if exists notification;
create table notification(
id int(8) primary key,
object_type varchar(64),
line varchar(64),
column varchar(64),
box varchar(64),
company_unit_id int(8),
user_id int(8),
created_timestamp int(8),
viewed_timestamp int(8),
is_viewed boolean,
title varchar(255),
text varchar(255),
company_id int(8)
);
alter table groups owner to postgres;


drop table if exists file;
create table file(
id int(8) primary key,
name varchar(128),
type varchar(128),
size int(8),
hash varchar(128),
is_deleted boolean,
file_binary_id int(8),
created_timestamp int(8),
created_by int(8),
updated_timestamp int(8),
updated_by int(8)
);
alter table groups owner to postgres;


drop table if exists file_routing;
create table file_routing(
id int(8) primary key,
file_id int(8),
table_name varchar(128),
table_id int(8),
type varchar(128)
);
alter table groups owner to postgres;


drop table if exists tempfiles;
create table tempfiles(
id int(8) primary key,
file_binary text,
file_binary_byte byte
);
alter table groups owner to postgres;


drop table if exists nomenclature;
create table nomenclature(
id int(8) primary key,
nomenclature_number varchar(128),
year int(4),
nomenclature_summary_id int(8),
company_unit_id int(8),
created_timestamp int(8),
created_by int(8),
updated_timestamp int(8),
updated_by int(8)
);
alter table groups owner to postgres;


drop table if exists nomenclature_summary;
create table nomenclature_summary(
id int(8) primary key,
number varchar(128),
year int(4),
company_unit_id int(8),
created_timestamp int(8),
created_by int(8),
updated_timestamp int(8),
updated_by int(8)
);
alter table groups owner to postgres;


drop table if exists case_index;
create table case_index(
id int(8) primary key,
case_index varchar(128),
title_ru varchar(128),
title_kz int(8),
title_en varchar(128),
storage_type int(4),
storage_year int(4),
note varchar(128),
company_unit_id int(8),
nomenclature_id int(8),
created_timestamp int(8),
created_by int(8),
updated_timestamp int(8),
updated_by int(8)
);


drop table if exists record;
create table record(
id int(8) primary key,
number varchar(128),
type int(4),
company_unit_id int(8),
created_timestamp int(8),
created_by int(8),
updated_timestamp int(8),
updated_by int(8)
);
alter table groups owner to postgres;


drop table if exists destruction_act;
create table destruction_act(
id int(8) primary key,
number varchar(128),
base varchar(128),
company_unit_id int(8),
created_timestamp int(8),
created_by int(8),
updated_timestamp int(8),
updated_by int(8)
);
alter table groups owner to postgres;

drop table if exists searchkey;
create table searchkey(
id int(8) primary key,
name varchar(128),
company_unit_id int(8),
created_timestamp int(8),
created_by int(8),
updated_timestamp int(8),
updated_by int(8)
);
alter table groups owner to postgres;


drop table if exists searchkey_routing;
create table searchkeyRouting(
id int(8) primary key,
searchkey_id int(8),
table_name varchar(128),
table_id int(8),
type varchar(128)
);
alter table groups owner to postgres;



